#include <RDA5807M.h>
// https://github.com/csdexter/RDA5807M

#include <avr/sleep.h>
#include <avr/io.h>
#include <EEPROM.h>

RDA5807M radio;
const uint8_t presetMode = 1; // 0 = automatic seek, 1 = preset tuning
const uint16_t presets[] = {
  8930, 9010, 9090, 9130,
  9190, 9330, 9650, 9770,
  9810, 9850, 9890, 9970,
  10110, 10330, 10610, 10650,
  10690, 10730
};
const uint8_t presetCount = 17;

uint8_t currFreq = 0;
uint8_t currVol = 0;

volatile uint8_t btnPush = 0;
#define leftBtn 1 // Real pin 3 (left button)
#define rightBtn 0 // Real pin 2 (right button)

void setup() {
  pinMode(leftBtn, INPUT_PULLUP);
  pinMode(rightBtn, INPUT_PULLUP);

  PORTA.DIRSET = PIN3_bm; // Real pin 7 (LED)
  PORTA.OUTSET = PIN3_bm; // Turn on LED
  delay(100);
  initRadio();
  //PORTA.OUTCLR = PIN3_bm; // Turn off LED
}

void loop() {
  if (btnPush > 0) {
    detachInterrupt(digitalPinToInterrupt(leftBtn));
    detachInterrupt(digitalPinToInterrupt(rightBtn));
    //PORTA.OUTSET = PIN3_bm; // Turn on LED
    byte count = 0;
    do {
      delay(100);
      count++;
    } while (((PORTA.IN & PIN7_bm) == 0) or ((PORTA.IN & PIN6_bm) == 0));

    if (count >= 2) {
      // Press and hold
      if (!presetMode) {
        // Seek
        if (btnPush == 1) radio.seekDown();
        if (btnPush == 2) radio.seekUp();
      } else {
        // Presets
        if (btnPush == 1) setPreset(0);
        if (btnPush == 2) setPreset(1);
      }
    } else {
      // Short press
      if (btnPush == 1) {
        radio.volumeDown();
        if (currVol > 1) { 
          currVol--;
          EEPROM.write(2, currVol);
        }
      }
      if (btnPush == 2) {
        radio.volumeUp();
        if (currVol < 15) { 
          currVol++;
          EEPROM.write(2, currVol);
        }
      }
    }
  }
  // Radio running, sleep until button pressed
  //PORTA.OUTCLR = PIN3_bm; // Turn off LED
  delay(100); // Necessary for stability
  sleep_enable();
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  attachInterrupt(digitalPinToInterrupt(leftBtn), wakeUp, CHANGE);
  attachInterrupt(digitalPinToInterrupt(rightBtn), wakeUp, CHANGE);
  sleep_cpu();
  sleep_disable();
}

void wakeUp() {
  if ((PORTA.IN & PIN7_bm) == 0) btnPush = 1;
  if ((PORTA.IN & PIN6_bm) == 0) btnPush = 2;
}

void initRadio() {
  // Turn on radio
  radio.begin(RDA5807M_BAND_WEST);

  // Read EEPROM
  currFreq = EEPROM.read(0);
  //currFreq = 255;
  if (currFreq == 255) {
    currFreq = 2; // 90.9MHz
    EEPROM.write(0, currFreq);
  }

  currVol = EEPROM.read(2);
  if (currVol > 15) {
    currVol = 1; // Default volume on first run
    EEPROM.write(2, currVol);
  }

  // Set freq
  delay(1000); // Necessary
  radio.setFrequency(presets[currFreq]);

  // Set initial volume to one for reference
  for (byte maxVol = 15; maxVol > 1; maxVol--) {
    delay(1);
    radio.volumeDown();
  }
  // Set real volume to currVol
  for (byte minVol = 1; minVol < currVol; minVol++) {
    delay(1);
    radio.volumeUp();
  }
}

void setPreset(bool dir) {
  if (dir == 0) {
    // Preset down
    if (currFreq != 0) {
      currFreq--;
    } else {
      currFreq = presetCount; // Wrap around
    }
  } else {
    // Preset up
    if (currFreq != presetCount) {
      currFreq++;
    } else {
      currFreq = 0; // Wrap around
    }
  }

  radio.setFrequency(presets[currFreq]);
  EEPROM.write(0, currFreq);
}
