# Tiny5807

A portable FM radio designed around the Attiny402 + RDA5807m.

Including schematics, gerbers, code, etc.

![Board screenshot](Screen Shot 2022-03-31 at 9.32.35 PM.png)

![Schematic screenshot](Schematic.png)
